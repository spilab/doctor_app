import { Component, OnInit } from '@angular/core';
import { EHR } from '../models/ehr';
import { ResourcesService } from '../services/resources.service';
import { BackendService } from '../services/backend.service';
import { NumberFormatStyle } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [
    ResourcesService,
    BackendService]
})
export class DashboardComponent implements OnInit {

  public ehrs:Array<any>;
  public patientId:String;
  public patiendIdSearched:String;
  public encontrado:boolean;
  public show_no_encontrado:boolean;
  public fileToUpload:any;

  constructor(  private resources_service : ResourcesService,
                private backend_service : BackendService) {
    this.ehrs = [];
    this.patientId = '';
    this.patiendIdSearched = '';
    this.encontrado = false;
    this.show_no_encontrado = true;

    this.fileToUpload = undefined;
  }

  ngOnInit() {

  }


  searchPatient(id:String):void{
    console.log("Cargando los EHRs desde el blockchain del paciente");
    if (id.replace(" ","")!=""){
      this.patiendIdSearched = id;
      this.cargarEHRs();
    } else{
      this.patientId = this.patiendIdSearched;
    }
  }

  cargarEHRs(){
    this.backend_service.loadEHRs(this.patiendIdSearched).subscribe(
      (res)=>{
        console.log(res);
        let ehrs=res.body;
        this.encontrado=true;
        this.show_no_encontrado=false;
        this.cargarResources(ehrs);
    },(err)=>{
      console.log(err);
      if (err.status==404){
        alert("Patient not found");
        this.encontrado=false;
        this.show_no_encontrado=true;
      }
      else{
        alert("Something go bad!! Sorry");
        this.encontrado=false;
      }
    });
  }


  cargarResources(ehrs):void{
    this.ehrs = [];
    for (var i=0; i<ehrs.length; i++){
      let block = ehrs[i];
      var t0 = performance.now();
      this.resources_service.getFile(block['urlresources'],block['fileid'],block['token']).subscribe((res)=>{
        var t1 = performance.now();
        console.log("Time to retrieve file: " + (t1 - t0) + " milliseconds.")
        console.log(res);
        let filename = res.headers.get('content-disposition').split("filename=")[1].split('"').join("");//["Content-Disposition"];
        console.log(filename);
        let filename_split = filename.split(".");
        let name = filename.replace("."+filename_split[filename_split.length-1],"");
        let type = res.headers.get('content-type');//["Content-Type"];
        let data = new Blob([res.body], { type: type });
        let ehr = new EHR(block['fileid'],block['token'],block['urlresources'], filename, name, type, data);
        this.ehrs.push(ehr);
        console.log(this.ehrs);
      },(err)=>{
        console.log(err);
        console.log("Resource with fileid "+block['fileid']+" not found");
      });
    }
  }


  download(ehr: EHR){
    //TODO coger el data y el filename del EHR y guardarlo
    console.log("Descargando EHR");
    console.log(ehr);

    var blob = ehr.data;

    // IE doesn't allow using a blob object directly as link href
    // instead it is necessary to use msSaveOrOpenBlob
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob);
        return;
    }

    // For other browsers: 
    // Create a link pointing to the ObjectURL containing the blob.
    const data = window.URL.createObjectURL(blob);
    let fileid = ehr.filename;
    var link = document.createElement('a');
    link.href = data;
    link.download = fileid;
    // this is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

    setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
  }

  postMethod(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  addEHR() {
    if (this.fileToUpload!=undefined){
      var t0 = performance.now();
      this.resources_service.saveFile(this.fileToUpload).subscribe((res)=>{
        var t1 = performance.now();
        console.log("Time to save file: " + (t1 - t0) + " milliseconds.")
        console.log(res);
        var data = res;
        var fileid = data['fileid'];
        var token = data['token'];
        this.backend_service.saveEHR(this.patiendIdSearched,fileid, token).subscribe((res)=>{
          alert("The new EHR has been added to the blockchain of the patient");
          this.cargarEHRs();
        },(err)=>{
          alert("[ERROR]: EHR hasn't been added to the blockchain of the patient. Sorry!!");
        })
      },(err)=>{
        alert("[ERROR]: EHR hasn't been added to the Information System of your organization. Sorry!!");
      });
    }
    else{
      alert("Please. Load a file previously!")
    }  
  }

}
