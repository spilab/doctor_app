export class EHR{
    constructor(
        /*valores obtenidos del blockchain del paciente */
      public fileid:string,
      public token:string,
      public URL_resources:string,
        /*valores obtenidos de la api de recursos */
      public filename:string,
      public name:string,
      public type:string,
      public data: any
    ){};
  }
  