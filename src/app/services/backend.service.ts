import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, observable } from 'rxjs';

import { GLOBAL } from './globals';

@Injectable()
export class BackendService {
    public url:string;
    public url_resources : string;

    constructor( private _http:HttpClient){
      this.url= GLOBAL.URL_BACKEND;
      this.url_resources = GLOBAL.URL_RESOURCES;
    }

    saveEHR(patientId:String, fileid:String, token:String): Observable<any>{
        console.log("Salvando un EHR del paciente "+patientId);
        console.log(patientId);
        let data= { 'fileid': fileid,
                    'token': token,
                    'urlresources':this.url_resources};
        let headers= new HttpHeaders({'Content-Type':'application/json'});
        return this._http.post(this.url+''+patientId,data,{headers: headers, observe : 'response'});
    }

    loadEHRs(patientId:String): Observable<any>{
        //TODO consultar en el blockchain del paciente todos los bloques
        console.log("Buscando el host y port del blockchain del paciente en el blockchain general");
        console.log(patientId);
        let headers= new HttpHeaders({'Content-Type':'application/json'});
        return this._http.get(this.url+''+patientId,{headers: headers, observe : 'response'});
    }
}
