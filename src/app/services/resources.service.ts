import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EHR } from '../models/ehr';
import { GLOBAL } from './globals';

@Injectable()
export class ResourcesService {
  public url:string;

  constructor( private _http:HttpClient){
    this.url= GLOBAL.URL_RESOURCES;
  }

  getFile(urlresources, fileid, token):Observable<any>{
      let headers= new HttpHeaders({'Authorization':token});
      return this._http.get(urlresources+'files/'+fileid,{headers: headers, responseType: 'blob', observe : 'response'});
      //return this.http.get(endpoint, { });

  }

  saveFile(file):Observable<any>{
    let formData = new FormData();
    console.log(file); 
    formData.append('file', file, file.name); 
    return this._http.post(this.url+'files/'+file.name, file);
  }
}
